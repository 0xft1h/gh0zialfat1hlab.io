---
layout    : post
title     : Playing With Django.pt2 Introduce App
date      : 2020-03-12 21:18:15 +0700
categories: ['python','django']
tags      : ['django', 'python']
author    : Muhammad Fahri
---

### Preface

This article series is intended for beginner.

> Goal: Make you understand about App in Django

Dengan mempelajari tutorial ini kita akan memahami tentang App pada django.

### Reading

Selalu lihat dokumentasi resmi terlebih dahulu sebelum anda memulai real case.

* [Django Tutorial](https://docs.djangoproject.com/en/3.0/intro/tutorial01/)

<br/>
### Lets begin to code
Sebelumnya kita sudah membahas tentang urls,views dan template saya harap kalian sudah memahaminya,
sekarang kita akan membahas tentang App.

##### Apa itu App di Django?

Setiap aplikasi yang kita tulis di Django terdiri dari paket Python yang mengikuti aturan tertentu. Django hadir dengan utilitas yang secara otomatis menghasilkan struktur direktori dasar suatu aplikasi, sehingga Kita dapat fokus pada penulisan kode daripada memikirkan direktori.

##### Projects vs Apps

Apa perbedaan antara project dan app? App adalah aplikasi web yang melakukan sesuatu sebagai contoh,Sistem Web blog, database catatan publik atau aplikasi polling sederhana.

Sedangkan Proyek adalah kumpulan konfigurasi dan App. Sebuah proyek dapat berisi beberapa App dan App dapat berada di beberapa project.

##### Membuat App

App sendiri bisa ditaruh dimana saja selama dia bisa terjangkau oleh Python path kita. Tapi dalam tutorial ini ki akan membuat appnya tepat berada dalam project kita dan berada di sebelah file <i class="fas fa-file-code"></i> `manage.py` Kita sehingga dapat diimpor sebagai modul top-level bukan sebagai submodule.

Untuk membuat App, pastikan kita berada didirektori yang sama dengan <i class="fas fa-file-code"></i> `manage.py` dan ketik perintah ini:
```
$ python manage.py startapp accounts
```
Perintah tersebut akan membuat direktori app kita, seperti ini bentuknya.
```
accounts
├── admin.py
├── apps.py
├── __init__.py
├── migrations
│   └── __init__.py
├── models.py
├── tests.py
└── views.py
```
Ket :
 - <i class="fas fa-file-code"></i> `manage.py` menjalankan program pengelola project kita.
 - `startapp` Merupakan perintah untuk membuat app baru.
 - `accounts` Merupakan nama app kita kita beri nama akun disini karena kita akan membuat custom django

Mari kita tulis views pertama kita. Buka file `accounts/views.py` dan tambahkan syntak code berikut:
```python
from django.http import HttpResponse


def index(request):
    return HttpResponse("Hello, world. You're at the accounts index.")
```
Ini hanyalah views sederhana kalian pasti sudah paham, untuk memanggil views tersebut, kita perlu memetakannya ke URL dan untuk ini kita perlu URLconf.

Untuk membuat URLconf dalam App accounts. kita buat file <i class="fas fa-file-code"></i> `urls.py`. dalam direktori App kita, dan sekarang tampilan direktori App kita akan terlihat seperti ini.

```
accounts
├── admin.py
├── apps.py
├── __init__.py
├── migrations
│   └── __init__.py
├── models.py
├── tests.py
├── urls.py
└── views.py

```

Dan dalam file <i class="fas fa-file-code"></i> `urls.py` tambahkan syntax berikut.

```python
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
]
```

Langkah selanjutnya adalah mengarahkan URL root ke `accounts.urls`. di `myweb/urls.py` import modul include dari yang terdapat dalam modul `django.urls` dan tambahkan kedalam urlpatterns list kita, seperti ini.

```python
from django.contrib import admin # Mengimport modul admin bawaan django dari django.contrib
from django.urls import path, include     # Mengimport path modul dan include modul dari django.urls 

# Listing tempat mendefinisikan url yang akan diakses dibrowser

urlpatterns = [
    path('accounts/', include('accounts.urls')),
    path('admin/', admin.site.urls),
]
```

Fungsi `include()` ini memungkinkan kita mengacu URLconf lain. Kapanpun Django menghadapi `include()`, Ia memotong bagian apapun dari URL yang cocok dengan titik itu dan mengirimkan string yang tersisa ke URLconf yang disertakan untuk diproses lebih lanjut.

Ide dibalik `include()` adalah untuk memudahkan plug and play URL. Karena app accounts memiliki URLconf merek sendiri `(accounts/urls.py)` mereka dapat ditempatkan dibawah `/accounts/`, atau di jalur root apapun dan App akan tetap bekerja.

> Kapan menggunakan `include()`
<br />
<br />
Kita menggunakan `include()` ketika kita ingin menyertakan urlpatterns lain. selain admin.site.urls

Anda sudah menghubungkan views dari App kedalam URLconf utama kita, Check apakah App kita berjalan atau tidak.

Jalankan django dengan perintah berikut:
```
$ python manage.py runserver
```
Buka http://localhost:8000/accounts/ di browser kita. Hasilnya akan menjadi seperti ini.
![django basic app ](/assets/images/artikel/django/basic-app-django.png){:class="img-responsive"}


##### Penjelasan yang belum dibahas pada bab sebelumnya.

Sebelumnya kita sudah pernah membahas URLconf tapi saya belum menjelaskan lebih jauh tentang URLconf.
bentuk umum dari URLpatterns adalah list seperti ini
```python
urlpatterns = [
    path('accounts/', include('accounts.urls')),
    path('admin/', admin.site.urls),
]
```

##### Pertama kita bahas tentang path()
path() adalah sebuah fungsi yang melewatkan 4 arguments, 2 dibutuhkan yaitu `route dan view` dan 2 bersifat optional `kwargs dan nama_route` kita akan membahas tentang argument-argument tersebut

1. **path() argument: route** <br/>
Route adalah sebuah string yang mengandung pola URL. Ketika memproses request. Django mula pada pola pertama di urlpatterns dan memasukannya kedaftar, membandingkan URL yang diminta dengan masing-masing pola hingga menemukan yang cocok.<br/><br/>
Pola tidak mencari parameter GET dan POST, atau nama domain.
Sebagai contoh, dalam sebuah request pada `https://www.example.com/myapp/`, URLconf akan mencari untuk `myapp/`. Dalam sebuah request pada `https://www.example.com/myapp/?page=3`, URLconf juga akan mencari `myapp/`.
2. **path() argument: view** <br />
Ketika Django menemukan pola yang cocok, dia memanggil fungsi yang didefinisikan didalamnya dengan sebuah object `HttpRequest` sebagai argumen pertama dan nilai-nilai apapun setelahnya dianggap sebagai kata kunci .
3. **path() argument: kwargs** <br />
Digunakan sebagai argumen kata kunci bisa juga melewatkan dictionary kedalam target view.
4. **path() argument: name**
Digunakan untuk menamai URL kita, kita butuh membuat nama khusus dalam URL kita khususnya ketika kita menggunakan template agar tidak bentrok dengan URL lain.


#### Referensi
---
1. [Django web](https://docs.djangoproject.com/id/3.0/intro/tutorial01/)