---
layout    : post
title     : Playing With Django.pt1 Introduce URL,Views & Templates
date      : 2020-03-12 23:18:15 +0700
categories: ['python','django']
tags      : ['django', 'python']
author    : Muhammad Fahri
---

### Preface

This article series is intended for beginner.

> Goal: Make you understand the concept of url and views

Dengan mempelajari tutorial ini kita akan memahami cara kerja url/router dan views pada Django.

### Reading

Selalu lihat dokumentasi resmi terlebih dahulu sebelum anda memulai real case.

* [Django URL dispatcher](https://docs.djangoproject.com/en/3.0/topics/http/urls/)

<br/>
### Lets begin to code

#### URL
Coba kita lihat pada tampilan awal project django yang sudah kita install sebelumnya.

![django web runing ](/assets/images/artikel/django/django-web-runing.png){:class="img-responsive"}

Terlihat disana tampilan awal homepage yang disediakan django selanjutnya kita akan mencoba memodifikasi homepage tersebut.

Buka file <i class="fas fa-file-code"></i> `urls.py` yang ada pada apps folder, `myweb`seperti gambar berikut.

![django default url ](/assets/images/artikel/django/django-default-url.png){:class="img-responsive"}

pada `urls.py`, terdapat syntax berikut:

```python
from django.contrib import admin # Mengimport modul admin bawaan django dari django.contrib
from django.urls import path     # Mengimport path modul dari django.urls 

# Listing tempat mendefinisikan url yang akan diakses dibrowser
urlpatterns = [
    path('admin/', admin.site.urls),
]
```

Kita tambahkan url untuk merewrite homepage django dan merubahnya sesuai tampilan kita, dan kita juga akan menambahkan url lagi agar lebih memahami konsep url pada django.

```python
from django.contrib import admin # Mengimport modul admin bawaan django dari django.contrib
from django.urls import path     # Mengimport path modul dari django.urls 
from django.http import HttpResponse # Mengimport modul Httpresponse dari modul django.http untuk menangani response http

# Membuat method view untuk yang akan dipanggil di url
def index(request):                     # Argumen request digunakan sebagai parameter response
    return HttpResponse('<h1>Hello World !!</h1>')  # Mereturn Httpresponse berupa string
def about(request):                     # Argumen request digunakan sebagai parameter response
    return HttpResponse('<h1>Welcome to about, my name is fahri !</h1>') # Mereturn Httpresponse berupa string
# Listing tempat mendefinisikan url yang akan diakses dibrowser
urlpatterns = [
    path('admin/', admin.site.urls),
    path('',index),
    path('about',about),
]
```
Hasilnya akan menjadi seperti berikut ini
![django helloworld ](/assets/images/artikel/django/helloworld-django.png){:class="img-responsive"}

![django hi about ](/assets/images/artikel/django/hi-about-django.png){:class="img-responsive"}

##### Explanation

> Ketika kita mengakses url pada browser, browser akan mengirimkan request url ke server dan server akan mencari url didalam file <i class="fas fa-file-code"></i> `urls.py` yang ada pada main folder `myweb`, kemudian server akan mencari url dalam list patterns mana yang sesuai dengan request, kemudian memanggil method view yang didefinisikan dalam urlpatterns.

#### Views

Selanjutnya kita akan merapihkan viewsnya sangat tidak disarankan menyatukan antara urls dan views, kita akan memisahkan viewsnya dari <i class="fas fa-file-code"></i> `urls.py`.

Pertama kita buat file <i class="fas fa-file-code"></i> `views.py` pada app folder myweb seperti berikut.

```
myweb
├── db.sqlite3
├── manage.py
└── myweb
    ├── asgi.py
    ├── __init__.py
    ├── __pycache__
    │   ├── __init__.cpython-38.pyc
    │   ├── settings.cpython-38.pyc
    │   ├── urls.cpython-38.pyc
    │   └── wsgi.cpython-38.pyc
    ├── settings.py
    ├── urls.py
    ├── views.py
    └── wsgi.py
```
Kemudian pindahkan method views yang ada pada file <i class="fas fa-file-code"></i> `urls.py` kedalam file <i class="fas fa-file-code"></i> `views.py` sehingga menjadi seperti berikut.

![django create views ](/assets/images/artikel/django/create-views-django.png){:class="img-responsive"}

Setelah itu kita edit file <i class="fas fa-file-code"></i> `urls.py` kita import views yang tadi kita buat kedalam urls kita seperti berikut.

```python
from django.contrib import admin # Mengimport modul admin bawaan django dari django.contrib
from django.urls import path     # Mengimport path modul dari django.urls 
#from .views import index,about #cara seperti ini tidak disarankan karena akan terjadi bentrok jika terdapat method dengan nama yang sama dalam views yang berbeda

from . import views # cara seperti ini akan membuat views menjadi namespace sehingga kita tahu method yang kita import milik views yang mana

# Listing tempat mendefinisikan url yang akan diakses dibrowser
urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.index),
    path('about',views.about),
]
```
Kita juga bisa membuat variable didalam views seperti berikut.
```python
from django.http import HttpResponse # Mengimport modul Httpresponse dari modul django.http untuk menangani response http

# Membuat method view untuk yang akan dipanggil di url
def index(request):                     # Argumen request digunakan sebagai parameter response
    judul = '<h1>Hello ini homepage !!</h1>' # mendefinisikan variable
    sub_judul = '<h2>Selamat datang diblogku</h2>'
    output = judul+sub_judul
    return HttpResponse(output)  # Mereturn Httpresponse berupa string
def about(request):                     # Argumen request digunakan sebagai parameter response
    return HttpResponse('<h1>Welcome to about, my name is fahri !</h1>') # Mereturn Httpresponse berupa string
```

Hasilnya akhirnya akan menjadi seperti ini.
![django create views ](/assets/images/artikel/django/basic-views-django-final.png){:class="img-responsive"}

Oke cukup sampai disini kita membahas basic URL dan views selanjutnya kita akan membahas template

#### Template

Pengenalan template dengan template kita akan menggunakan html sebagai views kita.

Pertama kita akan membuat template caranya adalah kita settings dulu nama direktori untuk template dengan mengedit file <i class="fas fa-file-code"></i> `settings.py`, pada bagian berikut.
```python
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
```
Ubah menjadi
```python
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'], # Nama direktori templates
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

```

Kemudian karena kita belum membuat app kita buat direktori template diluar apps folder `myweb` seperti berikut.

```
myweb
├── db.sqlite3
├── manage.py
├── myweb
│   ├── asgi.py
│   ├── __init__.py
│   ├── __pycache__
│   │   ├── __init__.cpython-38.pyc
│   │   ├── settings.cpython-38.pyc
│   │   ├── urls.cpython-38.pyc
│   │   ├── views.cpython-38.pyc
│   │   └── wsgi.cpython-38.pyc
│   ├── settings.py
│   ├── urls.py
│   ├── views.py
│   └── wsgi.py
└── templates
```

Kemudian kita coba tambahkan file <i class='fas fa-file-code'></i> `index.html` dan file <i class='fas fa-file-code'></i> `about.html` kedalam folder templates seperti berikut.
```
myweb
├── db.sqlite3
├── manage.py
├── myweb
│   ├── asgi.py
│   ├── __init__.py
│   ├── __pycache__
│   │   ├── __init__.cpython-38.pyc
│   │   ├── settings.cpython-38.pyc
│   │   ├── urls.cpython-38.pyc
│   │   ├── views.cpython-38.pyc
│   │   └── wsgi.cpython-38.pyc
│   ├── settings.py
│   ├── urls.py
│   ├── views.py
│   └── wsgi.py
└── templates
    ├── about.html
    └── index.html
```
Kemudian isi file <i class='fas fa-file-code'></i>`index.html` dengan syntax berikut.
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tutor Django Series</title>
</head>
<body>
    <h1>Selamat datang di websiteku</h1>
    <h3>Hello ini menggunakan template</h3>

    <ul>
        <li><a href="/about">About</a></li>
    </ul>
</body>
</html>
```
Dan file <i class='fas fa-file-code'></i>`about.html` dengan syntax berikut.
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tutor Django Series</title>
</head>
<body>
    <h1>About</h1>
    <h3>Hello ini menggunakan template</h3>

    <ul>
        <li><a href="/">Home</a></li>
    </ul>
</body>
</html>
```
Setelah itu edit file <i class='fas fa-file-code'></i>`views.py` menjadi seperti berikut.
```python
from django.http import HttpResponse # Mengimport modul Httpresponse dari modul django.http untuk menangani response http
from django.shortcuts import render # Mengimport modul render dari library django.shorcuts
# Membuat method view untuk yang akan dipanggil di url

def index(request):
    return render(request,'index.html') # Merender index.html
def about(request):                     
    return render(request, 'about.html') # Merender about.html
```

Hasilnya akhirnya akan menjadi seperti ini.

![django basic templates ](/assets/images/artikel/django/basic-django-template-exam1.png){:class="img-responsive"}
![django basic templates ](/assets/images/artikel/django/basic-django-template-exam2.png){:class="img-responsive"}

##### Explanation

> Ketika client merequest urls, dan memanggil sebuah views maka views tersebut akan dijalankan jika return views membutuhkan template maka views akan merender html berdasarkan request dari user.

Oke cukup sampai disini tutorial hari ini selanjutnya kita akan membahas app pada django, see you ...

# Referensi
---
1. [Django web](https://www.djangoproject.com/)
<br />
2. [Belajar Django kelas terbuka](https://www.youtube.com/watch?v=hPXNP1NoVNQ&list=PLZS-MHyEIRo6p_RwsWntxMO5QAqIHHHld&index=1)