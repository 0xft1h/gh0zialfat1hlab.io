---
layout    : post
title     : Introduce and setup installation Django
date      : 2020-03-12 22:18:15 +0700
categories: ['python','django']
tags      : ['django', 'python']
author    : Muhammad Fahri
---

# What is Django ?

Django adalah high-level web framework Python yang memiliki pengembangan cepat dalam keamanan dan perawatan website. Dibangun oleh pengembang berpengalaman, Django menjangkau banyak sekali kerumitan dalam pengembangan web, jadi kamu bisa fokus dalam menulis aplikasi tanpa harus menulis ulang jalurnya. Django dapat digunakan secara gratis dan open source, memiliki komunitas yang terus tumbuh dan aktif, dokumentasi yang jelas, dan banyak pilihan untuk dukungan yang gratis maupun berbayar. 

Django sendiri dibangun menggunakan konsep Model-View-Template (MVT),berbeda dengan framework web populer pada umumnya, seperti laravel dan codeiginiter yang menggunakan konsep Model-View-Controller.
## What is MVT?

Model View Template atau MVT adalah sebuah design pattern seperti MVC, design pattern MVT juga memisahkan kode menjadi tiga bagian.

![mvt-architecture](/assets/images/artikel/django/mvt-architecture.jpg){:class="img-responsive"}


* Model berhubungan dengan data dan interaksi ke database atau webservice.
* View bertindak sebagai penghubung data dan template.
* Template berhubungan dengan segala sesuatu yang akan ditampilkan ke end-user.


# Why Django ?

1. Sangat Cepat (Ridiculously fast).
Django dirancang untuk membantu developer membuat aplikasi dari konsep hingga penyelesaian secepat mungkin.
2. Keamanan yang meyakinkan (Reassuringly secure).
Django menganggap serius masalah keamanan dan membantu developer menghindari banyak kesalahan umum dalam keamaan web.
3. Sangat scalable (Exceedingly scalable).
Django menggunakan arsitektur berbasis komponen "shared-nothing" (masing-masing bagian dari arsitektur tidak bergantung pada yang lain, dan karenanya dapat diganti atau diubah jika diperlukan). Memiliki pemisahan yang jelas antara bagian-bagian yang berbeda berarti dapat meningkatkan lalu lintas yang meningkat dengan menambahkan perangkat keras pada level apapun: server caching, server database, atau server aplikasi. Beberapa situs tersibuk berhasil menyingkirkan Django untuk memenuhi permintaan mereka (misalnya Instagram dan Disqus, untuk memberi nama hanya dua).


# What you need to learn django ?

Peralatan yang kita butuhkan dalam membuat project Django adalah:

1. **Virtualenv**
2. **PIP**
<br />
<br />

#### Apa itu virtualenv ?

Virtualenv merupakan tool yang berfungsi membuat lingkungan virtual terisolasi. lingkungan python disini yang dimaksud meliputi binary (executable),library dan semua package yang di install oleh package manager python seperti pip dan easy_install.

Kenapa harus menggunakan virtualenv?
virtualenv menjaga lingkungan kita agar tidak bentrok dan merusak modul/package python yang terinstall secara global dalam sistem operasi kita.

#### Apa itu pip ?

PIP merupakan paket manajemen python yang digunakan untuk menginstall dan menghapus modul/package python, termasuk juga untuk menginstall django.

Instalasi pip dan virtualenv
##### Ubuntu & Debian
```
$ sudo apt-get install python-virtualenv python-pip python3-virtualenv python3-pip
```
##### Archlinux
```
$ sudo pacman -S python-pip python2-pip python2-virtualenv python-virtualenv
```
##### Windows
1. Unduh [get-pip.py](https://bootstrap.pypa.io/get-pip.py)
2. Buka command prompt dan arahkan ke folder dimana get-pip.py berada.
3. Jalankan perintah:
```
C:\> python get-pip.py
```
4. Dan pip sudah terinstall
Kamu bisa mengechecknya dengan cara ketikan perintah berikut pada command prompt:
```
$ pip -V
```
Kamu akan melihat output seperti ini:
```
C:\>pip 18.0 from c:\Programs\python\python37\lib\site-packages\pip (python 3.7)
```
Artinya pip berhasil terinstall kemudian install virtual env dengan perintah berikut:
```
C:\> pip install virtualenv
```


## Instalasi Django di virtualenv

Virtualenv adalah lingkungan virtual yang terisolasi dari lingkungan OS. Artinya kita bebas menginstall apapun di dalam lingkungan virtual, tanpa harus mengganggu aplikasi lain di luar lingkungan.

Kita bisa membuat lingkungan virtual dengan perintah berikut:
Sebelumnya kita buat directory dulu untuk menyimpan environtmentnya agar lebih rapih:

```
$ mkdir Dev
$ cd Dev/
$ virtualenv <nama-lingkungan-virtual>
```
![virtual install](/assets/images/artikel/django/virtualenv-install.png){:class="img-responsive"}


Sekarang kita punya direktori baru bernama Env, Aktivkan environtmentnya dengan perintah berikut:
```
$ source Env/bin/activate
```
![virtual aktivation](/assets/images/artikel/django/aktivasi-environtment.png){:class="img-responsive"}


Setelah itu install Django dengan perintah berikut:

```
$ pip install django
```
![django install](/assets/images/artikel/django/django-install.png){:class="img-responsive"}

Disini terlihat bahwa saya sudah menginstallnya.


## Membuat Project Django

Untuk membuat project baru Django, kita bisa ketik perintah berikut:

```
$ django-admin startproject myweb
```

Keterangan:
   * `startproject` adalah perintah untuk membuat project baru django  
   * `myweb` adalah nama projectnya
 
setelah itu, kita akan mendapatkan direktori baru bernama `myweb` dengan isi seperti ini:

![django create project ](/assets/images/artikel/django/django-create-project.png){:class="img-responsive"}


## Mengenal Struktur Direktori Project Django

Berikut ini adalah struktur direktori yang dibuatkan Django:
```
myweb
├── manage.py
└── myweb
    ├── asgi.py
    ├── __init__.py
    ├── settings.py
    ├── urls.py
    └── wsgi.py
```

Berikut file dan direktori yang harus kita ketahui:

* Direktori projects <i class="fas fa-folder"></i> `myweb/` adalah root direktori yang berisi seluruh file dari project. Nama direktori ini bisa diganti dengan apa saja, karena tidak akan jadi masalah bagai Django.

* File <i class="fas fa-file-code"></i> `manage.py` adalah program untuk mengelola project Django. Kita akan sering mengeksekusi 📄 `manage.py` saat ingin melakukan sesuatu terhadap project, misalnya: menjalankan server, melakukan migrasi, menciptakan sesuatu, dll.

* Direktori apps <i class="fas fa-folder"></i> `myweb/` adalah direktori yang berisi package untuk project kita, Nama direktori ini digunakan untuk mengimport apapun.

* File <i class="fas fa-file-code"></i> `myweb/__init__.py` adalah sebuah file kosong yang menyatakan direktori ini adalah sebuah paket (python package).

* File <i class="fas fa-file-code"></i> mysite/settings.py adalah tempat kita mengkonfigurasi project.

* File <i class="fas fa-file-code"></i> `myweb/urls.py` adalah tempat kita mendeklarasikan URL pada web kita.

* File <i class="fas fa-file-code"></i> `myweb/wsgi.py` adalah entri point untuk WSGI-compatible yang digunakan sebagai peladen server untuk project kita

* File <i class="fas fa-file-code"></i> `myweb/asgi.py` adalah entri point untuk ASGI-compatible yang digunakan sebagai peladen server untuk project kita.

# How Django Work ?

![django work ](/assets/images/artikel/django/basic-work-django.png){:class="img-responsive"}


* **`urls.py`**
<br />
Setiap request dari client akan diproses pertama kali oleh <i class="fas fa-file-code"></i> `urls.py`, karena di sini berisi definisi alamat URL (route) dan fungsi yang akan dieksekusi di setiap rute di akses.

* **`views.py`**
<br />
Views berfungsi untuk memproses request yang berasal dari <i class="fas fa-file-code"></i> `urls.py` kemudian mengembalikan response berdasarkan request dari client, jika client membutuhkan data maka views akan meminta ke <i class="fas fa-file-code"></i> `models.py` dan untuk UI/template akan meminta ke dalam folders template.

* **`models.py`**
<br />
Models adalah objek python yang mendefiniskan struktur data dari aplikasi, dan menyediakan mekanisme untuk mengelola sepert menambah,mengubah atau menghapus (CRUD) dan melakukan query records dalam database.

* **Templates**
<br />
Template digunakan untuk mendefiniskan struktur atau layout sebuah file (seperti halaman HTML). sebuah view bisa secara dinamis membuat sebuah html page dengan HTML template, dan mengisinya dengan data dari model. Template dapat digunakan untuk menentukan struktur jenis file apapun tidak harus HTML.


## Running Django.

Kita bisa menggunakan server yang disediakan djangoo untuk development.
Berikut ini cara menjalankannya, ketik perintah ini:
```
python manage.py runserver
```
Maka, server berhasil dijalankan seperti ini tampilannya.

![django server ](/assets/images/artikel/django/runing-django-server.png){:class="img-responsive"}

Abaikan alert yang berwarna merah, karena kita belum melakukan migrate.

Sekarang kita coba membuka django kita dibrowser buka `http://localhost:8000`, maka akan muncul tampilan seperti berikut.


![django web runing ](/assets/images/artikel/django/django-web-runing.png){:class="img-responsive"}

Tadaaaa.....
Kita sudah berhasil menginstal dan membuat project Django dengan benar.

# Whats Next ?
Selanjutnya kita akan melakukan pengenalan lebih jauh lagi tentang django seperti urls,views,dan template. See you ...


# Referensi
---
1. [Django web](https://www.djangoproject.com/)
<br />
2. [Developer mozilla Django Introduction](https://developer.mozilla.org/id/docs/Learn/Server-side/Django/Introduction)
<br />
3. [Petani Kode django untuk pemula](https://www.petanikode.com/django-untuk-pemula/#fn:2)
4. [geekinsta.com diffence between mvc and mvt](https://www.geekinsta.com/difference-between-mvc-and-mvt/)